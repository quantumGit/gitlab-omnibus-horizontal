# Networking Information

To get a complete list of ports used by the various components of the GitLab infrastructure, check the official documentation from [Gitlab](https://docs.gitlab.com/omnibus/package-information/defaults.html)

In this section, we list the ports that need to be open on the Redis server to make sure the machine can fulfill its role:

## Redis Server

|   **Component**    | **Protocol** |    **Port**    |    **Status**   |                                                   **Remote Subnets/IPs**                                                    |
| :----------------: | :----------: | :------------: | :-------------: | :-------------------------------------------------------------------------------------------------------------------------: |
|       Redis        |     TCP      |      6379      |    **Allowed**  | [prod-gitlab-wa01 IP], [prod-gitlab-wa02 IP], [prod-gitlab-wa03 IP], [prod-gitlab-rd01 IP], [prod-gitlab-rd02 IP], [prod-gitlab-rd03 IP], [prod-gitlab-cs01 IP], [prod-gitlab-cs02 IP], [prod-gitlab-cs03 IP]           |
|   SSH Management   |     TCP      |     22         |    **Allowed**  | [Management subnet]                                      |

To open those ports, run the following statements on `prod-gitlab-rd01`, `prod-gitlab-rd02`, and `prod-gitlab-rd03` servers:


```bash
    sudo ufw allow from [prod-gitlab-wa01 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-wa02 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-wa03 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-rd01 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-rd02 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-rd03 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 6379
    sudo ufw allow from [Management subnet] to any port 22
```