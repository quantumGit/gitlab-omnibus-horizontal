# Consul/Sentinel Role

Instructions from Gitlab for configuring [Consul](https://docs.gitlab.com/ee/administration/high_availability/database.html#configuring-the-consul-nodes)/[Sentinel](https://docs.gitlab.com/ee/administration/high_availability/redis.html#step-3-configuring-the-redis-sentinel-instances)

## Computers

| **Computer Name** | **DNS Name**              | **IP Address** |
| :---------------: | :-----------------------: | :------------: |
| prod-gitlab-cs01    | prod-gitlab-cs01.[domain.local] | [prod-gitlab-cs01 IP]    |
| prod-gitlab-cs02    | prod-gitlab-cs02.[domain.local] | [prod-gitlab-cs02 IP]    |
| prod-gitlab-cs03    | prod-gitlab-cs03.[domain.local] | [prod-gitlab-cs03 IP]    |

## Prerequisites

[Download/Install](https://about.gitlab.com/installation/#ubuntu) GitLab Omnibus on each node.  The steps are as follows.

1. Install and configure dependencies

    Command:
    ```bash
    sudo apt-get update
    sudo apt-get install -y curl openssh-server ca-certificates
    ```

2. Add the GitLab package repository

   Command:
    ``` BASH
    curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
    ```

3. Install package (Do not supply ``EXTERNAL_URL``)

    Command:
    ```Bash
    sudo apt-get install gitlab-ee
    ```

## Configuring the Sentinel nodes

### All Nodes

Repeat the steps below on all Sentinel nodes.

1. The redis password should be the same on all the redis nodes.

2. Open firewall for Redis and Sentinel.

    Command:
    ```bash
    sudo ufw allow 6379
    sudo ufw allow 26379
    ```

3. Edit the ``gitlab.rb`` file

    Command:
    ```bash
    sudo nano /etc/gitlab/gitlab.rb
    ```

    ```ruby
    roles ['redis_sentinel_role']

    # Must be the same in every sentinel node
    redis['master_name'] = 'gitlab-redis'

    # The same password for Redis authentication you set up for the master node.
    redis['master_password'] = 'Insert your password you created for Redis'

    # The IP of the master Redis node.
    redis['master_ip'] = '[prod-gitlab-rd01 IP]'

    # Define a port so Redis can listen for TCP requests which will allow other
    # machines to connect to it.
    redis['port'] = 6379

    # Port of master Redis server, uncomment to change to non default. Defaults
    # to `6379`.
    #redis['master_port'] = 6379

    ## Configure Sentinel
    sentinel['bind'] = 'SENTINEL_NODE_IP'

    # Port that Sentinel listens on, uncomment to change to non default. Defaults
    # to `26379`.
    # sentinel['port'] = 26379

    ## Quorum must reflect the amount of voting sentinels it take to start a failover.
    ## Value must NOT be greater then the amount of sentinels.
    ##
    ## The quorum can be used to tune Sentinel in two ways:
    ## 1. If a the quorum is set to a value smaller than the majority of Sentinels
    ##    we deploy, we are basically making Sentinel more sensible to master failures,
    ##    triggering a failover as soon as even just a minority of Sentinels is no longer
    ##    able to talk with the master.
    ## 1. If a quorum is set to a value greater than the majority of Sentinels, we are
    ##    making Sentinel able to failover only when there are a very large number (larger
    ##    than majority) of well connected Sentinels which agree about the master being down.s
    sentinel['quorum'] = 2

    ## Consider unresponsive server down after x amount of ms.
    # sentinel['down_after_milliseconds'] = 10000

    ## Specifies the failover timeout in milliseconds. It is used in many ways:
    ##
    ## - The time needed to re-start a failover after a previous failover was
    ##   already tried against the same master by a given Sentinel, is two
    ##   times the failover timeout.
    ##
    ## - The time needed for a slave replicating to a wrong master according
    ##   to a Sentinel current configuration, to be forced to replicate
    ##   with the right master, is exactly the failover timeout (counting since
    ##   the moment a Sentinel detected the misconfiguration).
    ##
    ## - The time needed to cancel a failover that is already in progress but
    ##   did not produced any configuration change (SLAVEOF NO ONE yet not
    ##   acknowledged by the promoted slave).
    ##
    ## - The maximum time a failover in progress waits for all the slaves to be
    ##   reconfigured as slaves of the new master. However even after this time
    ##   the slaves will be reconfigured by the Sentinels anyway, but not with
    ##   the exact parallel-syncs progression as specified.
    # sentinel['failover_timeout'] = 60000
    gitlab_rails['auto_migrate'] = false
    ```

4. To prevent database migrations from running on upgrade, run:

    Command:
    ```bash
    sudo touch /etc/gitlab/skip-auto-migrations
    ```
    > Only the primary GitLab application server should handle migrations.

5. Reconfigure Omnibus GitLab for the changes to take effect.

    Command:
    ```bash
    sudo gitlab-ctl reconfigure
    ```