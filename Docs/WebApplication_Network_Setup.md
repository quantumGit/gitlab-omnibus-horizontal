# Networking Information

To get a complete list of ports used by the various components of the GitLab infrastructure, check the official documentation from [Gitlab](https://docs.gitlab.com/omnibus/package-information/defaults.html)

In this section, we list the ports that need to be open on the GitLab Application server to make sure the machine can fulfill its role:

## Application Server

|   **Component**    | **Protocol** |    **Port**    |    **Status**   |            **Remote Subnets/IPs**               |
| :----------------: | :----------: | :------------: | :-------------: | :---------------------------------------------: |
|    GitLab Rails    |     TCP      |       443      |    **Allowed**  |                       Any                       |
|    GitLab Rails    |     TCP      |       80       |    **Allowed**  |                       Any                       |
|    GitLab Shell    |     TCP      |       22       |    **Allowed**  |                       Any                       |
|      Unicorn       |     TCP      |     Socket     |    **Allowed**  |
|       Redis        |     TCP      |     Socket     |    **Allowed**  |
|   SSH Management   |     TCP      |     22         |    **Allowed**  | [Management subnet] |

To open those ports, run the following statements on `prod-gitlab-wa01`, `prod-gitlab-wa02`, and `prod-gitlab-wa03` servers:


``` bash
    sudo ufw allow 22
    sudo ufw allow 80
    sudo ufw allow 443
```