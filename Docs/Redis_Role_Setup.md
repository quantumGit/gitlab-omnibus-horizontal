# Redis Role

Instructions from Gitlab for configuring [Reddis Cluster](https://docs.gitlab.com/ee/administration/high_availability/redis.html)

## Computers

| **Computer Name** | **DNS Name**              | **IP Address** |
| :---------------: | :-----------------------: | :------------: |
| prod-gitlab-rd01    | prod-gitlab-rd01.[domain.local] | [prod-gitlab-rd01 IP]    |
| prod-gitlab-rd02    | prod-gitlab-rd02.[domain.local] | [prod-gitlab-rd02 IP]    |
| prod-gitlab-rd03    | prod-gitlab-rd03.[domain.local] | [prod-gitlab-rd03 IP]    |

## Prerequisites

[Download/Install](https://about.gitlab.com/installation/#ubuntu) GitLab Omnibus on each node.  The steps are as follows.

1. Install and configure dependencies

    Command:
    ```bash
    sudo apt-get update
    sudo apt-get install -y curl openssh-server ca-certificates
    ```

2. Add the GitLab package repository

   Command:
    ``` BASH
    curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
    ```

3. Install package (Do not supply ``EXTERNAL_URL``)

    Command:
    ```Bash
    sudo apt-get install gitlab-ee
    ```

## Configuring the Redis nodes

### All Nodes

1. The redis password should be the same on all the redis nodes.

2. Open firewall for redis.

    Command:
    ```bash
    sudo ufw allow 6379
    ```

### Master Node

1. Edit the ``gitlab.rb`` file

    Command:
    ```bash
    sudo nano /etc/gitlab/gitlab.rb
    ```

    ```ruby
    # Specify server role as 'redis_master_role'
    roles ['redis_master_role']

    # IP address pointing to a local IP that the other machines can reach to.
    # You can also set bind to '0.0.0.0' which listen in all interfaces.
    # If you really need to bind to an external accessible IP, make
    # sure you add extra firewall rules to prevent unauthorized access.
    redis['bind'] = '[prod-gitlab-rd01 IP]'

    # Define a port so Redis can listen for TCP requests which will allow other
    # machines to connect to it.
    redis['port'] = 6379

    # Set up password authentication for Redis (use the same password in all nodes).
    redis['password'] = 'Insert your Redis Password'
    gitlab_rails['auto_migrate'] = false
    ```

2. Reconfigure Omnibus GitLab for the changes to take effect.

    Command:
    ```bash
    sudo gitlab-ctl reconfigure
    ```

### Slave Nodes

1. Edit the ``gitlab.rb`` file

    Command:
    ```bash
    sudo nano /etc/gitlab/gitlab.rb
    ```

    ```ruby
    # Specify server role as 'redis_slave_role'
    roles ['redis_slave_role']

    # IP address pointing to a local IP that the other machines can reach to.
    # You can also set bind to '0.0.0.0' which listen in all interfaces.
    # If you really need to bind to an external accessible IP, make
    # sure you add extra firewall rules to prevent unauthorized access.
    redis['bind'] = 'SERVER_IP'

    # Define a port so Redis can listen for TCP requests which will allow other
    # machines to connect to it.
    redis['port'] = 6379

    # The same password for Redis authentication you set up for the master node.
    redis['password'] = 'Insert your Redis Password'

    # The IP of the master Redis node.
    redis['master_ip'] = '[prod-gitlab-rd01 IP]'

    # Port of master Redis server, uncomment to change to non default. Defaults
    # to `6379`.
    #redis['master_port'] = 6379
    ```

2. To prevent database migrations from running on upgrade, run:

    Command:
    ```bash
    sudo touch /etc/gitlab/skip-auto-migrations
    ```
    > Only the primary GitLab application server should handle migrations.

3. Reconfigure Omnibus GitLab for the changes to take effect.

    Command:
    ```bash
    sudo gitlab-ctl reconfigure
    ```
