# Networking Information

To get a complete list of ports used by the various components of the GitLab infrastructure, check the official documentation from [Gitlab](https://docs.gitlab.com/omnibus/package-information/defaults.html)

In this section, we list the ports that need to be open on the PostgreSQL server to make sure the machine can fulfill its role:

## Database Server


|   **Component**    | **Protocol** |    **Port**    |    **Status**   |                               **Remote Subnets/IPs**                                 |
| :----------------: | :----------: | :------------: | :-------------: | :----------------------------------------------------------------------------------: |
|     PGBouncer      |     TCP      |      6432      |    **Allowed**  | [prod-gitlab-wa01 IP], [prod-gitlab-wa02 IP], [prod-gitlab-wa03 IP]                                                  |
|     PostgreSQL     |     TCP      |      5432      |    **Allowed**  | [prod-gitlab-db01 IP], [prod-gitlab-db02 IP], [prod-gitlab-db03 IP]                                                  |
|       Repmgr       |     TCP      | Socket (5432)  |    **Allowed**  | [prod-gitlab-db01 IP], [prod-gitlab-db02 IP], [prod-gitlab-db03 IP]                                                  |
|       Consul       |     TCP      |  8300 to 8500  |    **Allowed**  | [prod-gitlab-db01 IP], [prod-gitlab-db02 IP], [prod-gitlab-db03 IP], [prod-gitlab-cs01 IP], [prod-gitlab-cs02 IP], [prod-gitlab-cs03 IP]           |
|   SSH Management   |     TCP      |       22       |    **Allowed**  | [Management subnet]                                      |


To open those ports, run the following statements on `prod-gitlab-db01`, `prod-gitlab-db02`, and `prod-gitlab-db03` servers:


```bash
    sudo ufw allow from [prod-gitlab-wa01 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-wa02 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-wa03 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-db01 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-db02 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-db03 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-db01 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-db02 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-db03 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [Management subnet] to any port 22
```