# Machine Information

## Hardware Requirements

| **Machine Name** | **Machine Role** | **CPU** | **RAM** | **Disk** |
| :--------------: | :--------------: | :-----: | :-----: | :------: |
|  prod-gitlab-wa01| Web/Application  |    8    |  8 GB   |  64 GB   |
|  prod-gitlab-wa02| Web/Application  |    8    |  8 GB   |  64 GB   |
|  prod-gitlab-wa03| Web/Application  |    8    |  8 GB   |  64 GB   |
|  prod-gitlab-rd01|      Reddis      |    8    |  16 GB  |  64 GB   |
|  prod-gitlab-rd02|      Reddis      |    8    |  16 GB  |  64 GB   |
|  prod-gitlab-rd03|      Reddis      |    8    |  16 GB  |  64 GB   |
|  prod-gitlab-db01|    PostgreSQL    |    8    |  8 GB   |  128 GB  |
|  prod-gitlab-db02|    PostgreSQL    |    8    |  8 GB   |  128 GB  |
|  prod-gitlab-db03|    PostgreSQL    |    8    |  8 GB   |  128 GB  |
|  prod-gitlab-cs01| Consul/Sentinel  |    8    |  8 GB   |  64 GB   |
|  prod-gitlab-cs02| Consul/Sentinel  |    8    |  8 GB   |  64 GB   |
|  prod-gitlab-cs03| Consul/Sentinel  |    8    |  8 GB   |  64 GB   |