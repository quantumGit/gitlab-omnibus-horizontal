# Networking Information

## Ports

To get a complete list of ports used by the various components of the GitLab infrastructure, check the official documentation from [Gitlab](https://docs.gitlab.com/omnibus/package-information/defaults.html)

## Firewall Rules per Role

In this section, we list the ports that need to be open on each server to make sure the machine can fulfill its role:

### Application Server

|   **Component**    | **Protocol** |    **Port**    |    **Status**   |            **Remote Subnets/IPs**               |
| :----------------: | :----------: | :------------: | :-------------: | :---------------------------------------------: |
|    GitLab Rails    |     TCP      |       443      |    **Allowed**  |                       Any                       |
|    GitLab Shell    |     TCP      |       22       |    **Allowed**  |                       Any                       |
|      Unicorn       |     TCP      |     Socket     |    **Allowed**  |
|       Redis        |     TCP      |     Socket     |    **Allowed**  |
|   SSH Management   |     TCP      |     22         |    **Allowed**  |            Your Management subnets              |

To open those ports, run the following statements on `prod-gitlab-wa01`, `prod-gitlab-wa02`, and `prod-gitlab-wa03` servers:


``` bash
    sudo ufw allow 22
    sudo ufw allow 443
```

### Redis Server

|   **Component**    | **Protocol** |    **Port**    |    **Status**   |                                                   **Remote Subnets/IPs**                                                    |
| :----------------: | :----------: | :------------: | :-------------: | :-------------------------------------------------------------------------------------------------------------------------: |
|       Redis        |     TCP      |      6379      |    **Allowed**  | Application servers, Consul/Sentinel servers, and Redis servers          |
|   SSH Management   |     TCP      |     22         |    **Allowed**  | Your Management subnets                                   |

To open those ports, run the following statements on `prod-gitlab-rd01`, `prod-gitlab-rd02`, and `prod-gitlab-rd03` servers (Make sure you replace the placeholders with the server IP):


```bash
    sudo ufw allow from [prod-gitlab-wa01 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-wa02 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-wa03 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-rd01 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-rd02 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-rd03 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 6379
    sudo ufw allow from [management-subnet] to any port 22
```

### Consul/Sentinel Server


|   **Component**    | **Protocol** |    **Port**    |    **Status**   |                                                   **Remote Subnets/IPs**                                                    |
| :----------------: | :----------: | :------------: | :-------------: | :-------------------------------------------------------------------------------------------------------------------------: |
|       Redis        |     TCP      |      6379      |    **Allowed**  | [prod-gitlab-rd01 IP], [prod-gitlab-rd02 IP], [prod-gitlab-rd03 IP]          |
|     Sentinel       |     TCP      |      26379     |    **Allowed**  | Application servers, Consul/Sentinel servers           |
|       Consul       |     TCP      |  8300 to 8500 |    **Allowed**  | Database servers, Consul/Sentinel servers                                                |
|   SSH Management   |     TCP      |     22         |    **Allowed**  | Management subnets                                  |

To open those ports, run the following statements on `prod-gitlab-cs01`, `prod-gitlab-cs02`, and `prod-gitlab-cs03` servers:


```bash
    sudo ufw allow from [prod-gitlab-rd01 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-rd02 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-rd03 IP] to any port 6379
    sudo ufw allow from [prod-gitlab-wa01 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-wa02 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-wa03 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-db01 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-db02 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-db03 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [Management subnet] to any port 22
```

### Database Server


|   **Component**    | **Protocol** |    **Port**    |    **Status**   |                               **Remote Subnets/IPs**                                 |
| :----------------: | :----------: | :------------: | :-------------: | :----------------------------------------------------------------------------------: |
|     PGBouncer      |     TCP      |      6432      |    **Allowed**  | [prod-gitlab-wa01 IP], [prod-gitlab-wa02 IP], [prod-gitlab-wa03 IP], [prod-gitlab-db01 IP], [prod-gitlab-db02 IP], [prod-gitlab-db03 IP], [prod-gitlab-rd01 IP], [prod-gitlab-rd02 IP], [prod-gitlab-rd03 IP], [prod-gitlab-cs01 IP], [prod-gitlab-cs02 IP], [prod-gitlab-cs03 IP]           |
|     PostgreSQL     |     TCP      |      5432      |    **Allowed**  | [prod-gitlab-db01 IP], [prod-gitlab-db02 IP], [prod-gitlab-db03 IP], [prod-gitlab-rd01 IP], [prod-gitlab-rd02 IP], [prod-gitlab-rd03 IP], [prod-gitlab-cs01 IP], [prod-gitlab-cs02 IP], [prod-gitlab-cs03 IP]           |
|       Repmgr       |     TCP      | Socket (5432)  |    **Allowed**  | [prod-gitlab-db01 IP], [prod-gitlab-db02 IP], [prod-gitlab-db03 IP]                                                  |
|       Consul       |     TCP      |  8300 and 8500 |    **Allowed**  | [prod-gitlab-cs01 IP], [prod-gitlab-cs02 IP], [prod-gitlab-cs03 IP]                                                |
|   SSH Management   |     TCP      |       22       |    **Allowed**  | Management subnet                                   |


To open those ports, run the following statements on `prod-gitlab-db01`, `prod-gitlab-db02`, and `prod-gitlab-db03` servers:


```bash
    sudo ufw allow from [prod-gitlab-wa01 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-wa02 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-wa03 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-db01 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-db02 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-db03 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-rd01 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-rd02 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-rd03 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 6432
    sudo ufw allow from [prod-gitlab-db01 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-db02 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-db03 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-rd01 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-rd02 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-rd03 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 5432
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 8300
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 8300
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 8300
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 8500
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 8500
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 8500
    sudo ufw allow from [Management subnet] to any port 22
```