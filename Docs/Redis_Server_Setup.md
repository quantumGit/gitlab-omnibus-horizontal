# Redis Server Setup

## Computers

| **Computer Name** |       **DNS Name**        | **IP Address** |
| :---------------: | :-----------------------: | :------------: |
|  prod-gitlab-rd01   | prod-gitlab-rd01.[domain.local] |  [prod-gitlab-rd01 IP]   |
|  prod-gitlab-rd02   | prod-gitlab-rd02.[domain.local] |  [prod-gitlab-rd02 IP]   |
|  prod-gitlab-rd03   | prod-gitlab-rd03.[domain.local] |  [prod-gitlab-rd03 IP]   |

## Configuration

### Change Hostname

1. Remote into server or access via console

2. Change the hostname

   Command:

   ```
   sudo hostname <NEW_HOSTNAME>
   ```

3. Open the hostname file and modify to use the new hostname

   Command:

   ```bash
   sudo nano /etc/hostname
   ```

4. restart the computer

   Command:

   ```bash
   sudo reboot now
   ```

### Set Static IP

1. Remote into server or access via console

2. Go to the netplan directory

   Command:

   ```bash
   cd /etc/netplan
   ```

3. Remove all files except for _01-netcfg.yaml_

4. Edit _01-netcfg.yaml_

   Command:

   ```bash
   sudo nano 01-netcfg.yaml
   ```

5. The file should look like the example below, modify to meet your needs

   ```yaml
    network
   version: 2
   renderer: networkd
    ethernets
   ens160:
     dhcp4: no
     dhcp6: no
     addresses: [SERVER_IP/NETWORK_MASK]
     gateway4: NETWORK_GATEWAY
     nameservers:
       search: [domain.local]
       addresses: [DNS_SERVERS_COMMA_SEPARATED]
   ```

6. Save your changes and apply

   Command:

   ```bash
   sudo netplan apply
   ```

7. Edit _99-sysctl.conf_ to disable ipv6

   Command:

   ```bash
   sudo nano /etc/sysctl.d/99-sysctl.conf
   ```

8. Add the following to the end of the file

   ```CONF
   net.ipv6.conf.all.disable_ipv6 = 1
   net.ipv6.conf.default.disable_ipv6 = 1
   net.ipv6.conf.lo.disable_ipv6 = 1
   ```

9. Restart the computer

   Command:

   ```bash
   sudo reboot now
   ```

### Join Domain

1. Install the following Packages

   - Realmd
   - sssd
   - sssd-tools
   - libnss-sss
   - libpam-sss
   - krb5-user
   - adcli
   - samba-common-bin

   Command:

   ```bash
   sudo apt install -y realmd sssd sssd-tools libnss-sss libpam-sss krb5-user adcli samba-common-bin
   ```

2. During the installation of the “krb5-user” you’ll be prompted for the domain name. Fill in with domain.local in capital letters.

3. Edit _krb5.conf_ file

   Command:

   ```bash
   sudo nano /etc/krb5.conf
   ```

4. Add the following two entries to _libdefaults_

   ```conf
   dns_lookup_realm = true
   dns_lookup_kdc = true
   ```

5. Edit _timesyncd.conf_ file

   Command:

   ```bash
   sudo nano /etc/systemd/timesyncd.conf
   ```

6. Add the following 2 entries

   ```conf
   [Time]
   NTP=NTP Server IP
   FallbackNTP=[Secondary NTP Server IP]
   ```

7. Run the foll0wing to sync time

   Command: (Set NTP sync to true)

   ```bash
   sudo timedatectl set-ntp true
   ```

   Command: (Restart service)

   ```bash
   sudo systemctl restart systemd-timesyncd.service
   ```

   Command: (Force sync)

   ```bash
   sudo timedatectl --adjust-system-clock
   ```

8. Edit _realmd.conf_ file

   Command:

   ```bash
   sudo nano /etc/realmd.conf
   ```

9. The file should look like the example below, modify to meet your needs

   ```CONF
   [users]
    default-home = /home/%D/%U
    default-shell = /bin/bash

   [active-directory]
    default-client = sssd
    os-name = Ubuntu Server
    os-version = 18.04

   [service]
    automatic-install = no

   [domain.local]
    fully-qualified-names = yes
    automatic-id-mapping = no
    user-principal = yes
    manage-system = yes
   ```

10. Use the _pam-auth-update_ tool to set "Create home directory on login"

    Command:

    ```bash
    sudo pam-auth-update
    ```

11. Test settings, no errors should happen

    Command:

    ```bash
    realm discover domain.local
    ```

    Command:

    ```bash
    kinit <AD_USERNAME>
    ```

    Command:

    ```bash
    klist
    ```

    Command:

    ```bash
    kdestroy
    ```

12. Join Domain, if successful you should receive the message _Successfully enrolled machine in realm_

    Command:

    ```bash
    sudo realm join --verbose --user=<AD_JOIN_ACCOUNT> --computer-ou=OU=Gitlab,dc=domain,dc=local domain.local --install=/
    ```
