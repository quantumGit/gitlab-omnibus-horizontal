# Networking Information

To get a complete list of ports used by the various components of the GitLab infrastructure, check the official documentation from [Gitlab](https://docs.gitlab.com/omnibus/package-information/defaults.html)

In this section, we list the ports that need to be open on the Consul/Sentinel server to make sure the machine can fulfill its role:

## Consul/Sentinel Server


|   **Component**    | **Protocol** |    **Port**    |    **Status**   |                                                   **Remote Subnets/IPs**                                                    |
| :----------------: | :----------: | :------------: | :-------------: | :-------------------------------------------------------------------------------------------------------------------------: |
|     Sentinel       |     TCP      |      26379     |    **Allowed**  | [prod-gitlab-wa01 IP], [prod-gitlab-wa02 IP], [prod-gitlab-wa03 IP], [prod-gitlab-cs01 IP], [prod-gitlab-cs02 IP], [prod-gitlab-cs03 IP]           |
|       Consul       |     TCP      |  8300 to 8500  |    **Allowed**  | [prod-gitlab-db01 IP], [prod-gitlab-db02 IP], [prod-gitlab-db03 IP], [prod-gitlab-cs01 IP], [prod-gitlab-cs02 IP], [prod-gitlab-cs03 IP]                                                |
|   SSH Management   |     TCP      |     22         |    **Allowed**  | [Management subnet]                                      |

To open those ports, run the following statements on `prod-gitlab-cs01`, `prod-gitlab-cs02`, and `prod-gitlab-cs03` servers:


```bash
    sudo ufw allow from [prod-gitlab-wa01 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-wa02 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-wa03 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 26379
    sudo ufw allow from [prod-gitlab-db01 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-db02 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-db03 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-cs01 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-cs02 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [prod-gitlab-cs03 IP] to any port 8300:8500 proto tcp
    sudo ufw allow from [Management subnet] to any port 22
```