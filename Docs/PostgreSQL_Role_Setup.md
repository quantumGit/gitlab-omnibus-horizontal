# PostgreSQL Role

Instructions from Gitlab for configuring [PostgreSQL Cluster](https://docs.gitlab.com/ee/administration/high_availability/database.html#installing-omnibus-gitlab)

## Computers

| **Computer Name** | **DNS Name**              | **IP Address** |
| :---------------: | :-----------------------: | :------------: |
| prod-gitlab-db01    | prod-gitlab-db01.[domain.local] | [prod-gitlab-db01 IP]    |
| prod-gitlab-db02    | prod-gitlab-db02.[domain.local] | [prod-gitlab-db02 IP]    |
| prod-gitlab-db03    | prod-gitlab-db03.[domain.local] | [prod-gitlab-db03 IP]    |

## Prerequisites

[Download/Install](https://about.gitlab.com/installation/#ubuntu) GitLab Omnibus on each node.  The steps are as follows.

1. Install and configure dependencies

    Command:
    ```bash
        sudo apt-get update
        sudo apt-get install -y curl openssh-server ca-certificates
    ```

2. Add the GitLab package repository

    Command:
    ```bash
        curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
    ```

3. Install package (Do not supply ``EXTERNAL_URL``)

    Command:
    ```Bash
        sudo apt-get install gitlab-ee
    ```


4. Edit`/etc/gitlab/gitlab.rb`. Replace the content with the following configuration

    ```ruby
        # Disable all components except PostgreSQL
        roles ['postgres_role']

        # PostgreSQL configuration
        postgresql['listen_address'] = '0.0.0.0'
        postgresql['hot_standby'] = 'on'
        postgresql['wal_level'] = 'replica'
        postgresql['shared_preload_libraries'] = 'repmgr_funcs'

        # Disable automatic database migrations
        gitlab_rails['auto_migrate'] = false

        # Configure the consul agent
        consul['services'] = %w(postgresql)

        postgresql['pgbouncer_user_password'] = 'Insert the password for pgbouncer'
        postgresql['sql_user_password'] = 'Insert password for the SQL user'
        postgresql['max_wal_senders'] = 4

        postgresql['trust_auth_cidr_addresses'] = %w([Insert your GitLab Subnet] 127.0.0.1/32 0.0.0.0/32)
        repmgr['trust_auth_cidr_addresses'] = %w([Insert your GitLab Subnet] 127.0.0.1/32 0.0.0.0/32)

        consul['configuration'] = {
            retry_join: %w([prod-gitlab-db01 IP] [prod-gitlab-db02 IP] [prod-gitlab-db03 IP])
        }
    ```

5. Finally, reconfigure the primary node by running the following command:

    Command:
    ```Bash
        sudo gitlab-ctl reconfigure
    ```

6. Configure the secondary nodes by replacing the gitlab.rb file with the following content:

    ```ruby
        # Disable all components except PostgreSQL
        roles ['postgres_role']

        # PostgreSQL configuration
        postgresql['listen_address'] = '0.0.0.0'
        postgresql['hot_standby'] = 'on'
        postgresql['wal_level'] = 'replica'
        postgresql['shared_preload_libraries'] = 'repmgr_funcs'

        # Disable automatic database migrations
        gitlab_rails['auto_migrate'] = false

        # Configure the consul agent
        consul['services'] = %w(postgresql)

        postgresql['pgbouncer_user_password'] = 'Insert the password for pgbouncer'
        postgresql['sql_user_password'] = 'Insert password for the SQL user'
        postgresql['max_wal_senders'] = 4

        postgresql['trust_auth_cidr_addresses'] = %w([Insert your GitLab Subnet] 127.0.0.1/32 0.0.0.0/32)
        repmgr['trust_auth_cidr_addresses'] = %w([Insert your GitLab Subnet] 127.0.0.1/32 0.0.0.0/32)

        # HA setting to specify if a node should attempt to be master on initialization
        repmgr['master_on_initialization'] = false

        consul['configuration'] = {
        retry_join: %w([prod-gitlab-cs01 IP] [prod-gitlab-cs02 IP] [prod-gitlab-cs03 IP])
        }
    ```

7. Reconfigure gitlab by running the following command on the secondary nodes:

    Command:
    ```Bash
        sudo gitlab-ctl reconfigure
    ```

8. Make sure that nodes can resolve each others' hostnames. To do so, edit the hosts file `/etc/hosts` in each node and add the following information:

    Command:
    ```Bash
        [prod-gitlab-db01 IP] prod-gitlab-db01
        [prod-gitlab-db02 IP] prod-gitlab-db02
        [prod-gitlab-db03 IP] prod-gitlab-db03
    ```

    Your hosts file should like the following:

    ```Conf
        127.0.0.1       localhost.localdomain   localhost
        ::1             localhost6.localdomain6 localhost6
        [prod-gitlab-db01 IP]     prod-gitlab-db01
        [prod-gitlab-db02 IP]     prod-gitlab-db02
        [prod-gitlab-db03 IP]     prod-gitlab-db03
        # The following lines are desirable for IPv6 capable hosts
        ::1     localhost ip6-localhost ip6-loopback
        fe00::0 ip6-localnet
        ff02::1 ip6-allnodes
        ff02::2 ip6-allrouters
        ff02::3 ip6-allhosts
    ```

9. Connect to your primary PostgreSQL node, and open the database prompt:

    Command:
    ```Bash
        gitlab-psql -d gitlabhq_production
    ```

10. Enable the `pg_trgm` extension

    Command:
    ```Bash
        CREATE EXTENSION pg_trgm;
    ```

11. Exit the database prompt by running the command `\q`

12. View the PostgreSQL cluster to make sure it is initialized:

    Command:
    ```Bash
        gitlab-ctl repmgr cluster show
    ```
    The result should look like the following:

    ```Bash
        Role      | Name           | Upstream       | Connection String
        ----------+----------------|----------------|------------------------------------------------------------------------------------------------------
        * master  | prod-gitlab-db01 |                | host=prod-gitlab-db01 port=5432 user=gitlab_repmgr dbname=gitlab_repmgr sslmode=prefer sslcompression=0
    ```

13. Add the secondary nodes (prod-gitlab-db02 and prod-gitlab-db03) to the cluster by running the following command on each node:

    Command:
    ```Bash
        gitlab-ctl repmgr standby setup prod-gitlab-db01
    ```

    The process will take sometime but it will  look like the following:

    ```Bash
        Doing this will delete the entire contents of /var/opt/gitlab/postgresql/data
        If this is not what you want, hit Ctrl-C now to exit
        To skip waiting, rerun with the -w option
        Sleeping for 30 seconds
        Stopping the database
        Removing the data
        Cloning the data
        Starting the database
        Registering the node with the cluster
        ok: run: repmgrd: (pid 17168) 0s
    ```

14. Verify on the primary node that the cluster now contains all PostgreSQL nodes:

    Command:
    ```Bash
        gitlab-ctl repmgr cluster show
    ```

    The result should look like the following:

    ```Bash
        Role      | Name           | Upstream       | Connection String
        ----------+----------------|----------------|------------------------------------------------------------------------------------------------------
        * master  | prod-gitlab-db01 |                | host=prod-gitlab-db01 port=5432 user=gitlab_repmgr dbname=gitlab_repmgr sslmode=prefer sslcompression=0
          standby | prod-gitlab-db02 | prod-gitlab-db01 | host=prod-gitlab-db02 port=5432 user=gitlab_repmgr dbname=gitlab_repmgr sslmode=prefer sslcompression=0
          standby | prod-gitlab-db03 | prod-gitlab-db01 | host=prod-gitlab-db03 port=5432 user=gitlab_repmgr dbname=gitlab_repmgr sslmode=prefer sslcompression=0
    ```

15. Before moving on, make sure the databases are configured correctly. Run the following command on the primary node to verify that replication is working properly:

    Command:
    ```Bash
        gitlab-ctl repmgr cluster show
    ```
    The output should be similar to:

    ```Bash
        Role      | Name           | Upstream       | Connection String
        ----------+----------------|----------------|------------------------------------------------------------------------------------------------------
        * master  | prod-gitlab-db01 |                | host=prod-gitlab-db01 port=5432 user=gitlab_repmgr dbname=gitlab_repmgr sslmode=prefer sslcompression=0
          standby | prod-gitlab-db02 | prod-gitlab-db01 | host=prod-gitlab-db02 port=5432 user=gitlab_repmgr dbname=gitlab_repmgr sslmode=prefer sslcompression=0
          standby | prod-gitlab-db03 | prod-gitlab-db01 | host=prod-gitlab-db03 port=5432 user=gitlab_repmgr dbname=gitlab_repmgr sslmode=prefer sslcompression=0
    ```

    Also, check that the check master command works successfully on each node:

    Command:
    ```Bash
        su - gitlab-consul
        gitlab-ctl repmgr-check-master || echo 'This node is a standby repmgr node'
    ```


## Configuring the PGBouncer

The PGBouncer runs on each database node to serve as a frontend for psql connections. PGBouncer allows PostgreSQL nodes to handle large number of simultaneous connections by acting as a lightweight connection pooler for PostgreSQL. For more information, [read the wiki](https://wiki.postgresql.org/wiki/PgBouncer)

### Needed Values

| **Name**                  | **Value**   |
| :-----------------------: | :---------: |
| PGBOUNCER\_USERNAME       | pgbouncer |
| PGBOUNCER\_PASSWORD       | PASSWORD_FOR_PGBOUNCER  |
| PGBOUNCER\_PASSWORD\_HASH | HASH_PASSWORD_FOR_PGBOUNCER |
| PGBOUNCER\_NODE           | [prod-gitlab-db01 IP] |

- PGBOUNCER_USERNAME. Defaults to **_pgbouncer_**
- PGBOUNCER_PASSWORD. This is a password for pgbouncer service.
- PGBOUNCER_PASSWORD_HASH. This is a hash generated out of pgbouncer username/password pair. Can be generated with:

On `prod-gitlab-db01`, modify the file `/etc/gitlab/gitlab.rb` by adding the following lines:

1. Modify the role list by adding the pgbouncer role:

    ```ruby
    roles ['postgres_role', 'pgbouncer_role']
    ```

2. Then, add the following lines:

    ```ruby
    # Configure Pgbouncer
    pgbouncer['admin_users'] = %w(pgbouncer gitlab-consul)
    pgbouncer['auth_type'] = 'trust'
    pgbouncer['listen_addr'] = '0.0.0.0'

    # Configure Consul agent
    consul['watchers'] = %w(postgresql)

    pgbouncer['databases'] = {
        gitlabhq_production: {
        host: '0.0.0.0',
        user: 'pgbouncer',
        password: 'PGBouncer password'
        }
    }

    # START user configuration
    pgbouncer['users'] = {
        'gitlab-consul': {
            password: 'Gitlab Consul password'
        },
        'pgbouncer': {
            password: 'PGBouncer password'
        }
    }
    postgresql['md5_auth_cidr_addresses'] = %w([GitLab Subnet])

    pgbouncer['auth_type'] = 'trust'
    pgbouncer['listen_addr'] = '0.0.0.0'
    pgbouncer['listen_port'] = '6432'

    ```
3. Reconfigure gitlab by running the following command:

    Command:
    ```Bash
        gitlab-ctl reconfigure
    ```

4. Create a .pgpass file so Consul can be able to reload pgbouncer. You will be asked to enter the password for pgbouncer that you created:

    Command:
    ```Bash
        gitlab-ctl write-pgpass --host 127.0.0.1 --database pgbouncer --user pgbouncer --hostuser gitlab-consul
    ```

5. Verify that the pgbouncer is working by ensuring that the node is talking to the master:

    Command:
    ```bash
        gitlab-ctl pgb-console # You will be prompted for PGBOUNCER_PASSWORD
        show databases ; show clients ;
    ```

    Then, view the databases and clients to make sure they exist. The output should look like the following:

    ```bash
                name         | host | port |      database       | force_user | pool_size | reserve_pool | pool_mode | max_connections | current_connections | paused | disabled
        ---------------------+------+------+---------------------+------------+-----------+--------------+-----------+-----------------+---------------------+--------+----------
         gitlabhq_production |      | 5432 | gitlabhq_production |            |       100 |            5 |           |               0 |                   0 |      0 |        0
         pgbouncer           |      | 6432 | pgbouncer           | pgbouncer  |         2 |            0 | statement |               0 |                   0 |      0 |        0
        (2 rows)

        type |   user    | database  | state  |   addr    | port  | local_addr | local_port |    connect_time     |    request_time     | wait | wait_us |      ptr       | link | remote_pid | tls
        ------+-----------+-----------+--------+-----------+-------+------------+------------+---------------------+---------------------+------+---------+----------------+------+------------+-----
        C    | pgbouncer | pgbouncer | active | unix      |  6432 | unix       |       6432 | 2018-09-25 20:54:26 | 2018-09-25 20:54:26 |  709 |    1743 | 0x563df28cfce0 |      |       3712 |
        C    | pgbouncer | pgbouncer | active | 127.0.0.1 | 42762 | 127.0.0.1  |       6432 | 2018-09-25 21:06:06 | 2018-09-25 21:06:15 |    0 |     200 | 0x563df28cfe70 |      |          0 |
        (2 rows)

    ```

6. Configure the application servers by adding the following lines to the `/etc/gitlab/.gitlab/rb`:

    ```ruby
        # Disable PostgreSQL on the application node
        postgresql['enable'] = false

        gitlab_rails['db_host'] = '[prod-gitlab-db01 IP]'
        gitlab_rails['db_port'] = 6432
        gitlab_rails['db_password'] = 'Insert your SQL DB user password'
        gitlab_rails['auto_migrate'] = false
    ```

7. Run the GitLab reconfiguration on each application node:

    Command:
    ```bash
            gitlab-ctl reconfigure
    ```

8. Finally, make sure Consul is configured with the following information:

### Needed Values

| **Name**                   | **Value**   |
| :------------------------: | :---------: |
| CONSUL\_SERVER\_NODES      | [prod-gitlab-cs01 IP] [prod-gitlab-cs02 IP] [prod-gitlab-cs03 IP] |
| PGBOUNCER\_PASSWORD\_HASH  | [PGBOUNCER_PASSWORD_HASH] |
| POSTGRESQL\_PASSWORD\_HASH | [POSTGRESQL_PASSWORD_HASH] |
| Number of DB Nodes         | 4 |
| Network Address            | [GitLab_SUBNET] |

### Conclusion

By the end of the configuration, omnibus will have the following login/group roles created in each database server:

* gitlab
* gitlab-consul
* gitlab-psql
* gitlab_replicator
* gitlab_repmgr
* pgbouncer
* pg_signal_backend (group)