# Web Application Role Setup

Instructions from Gitlab for configuring [GitLab HA Cluster](https://docs.gitlab.com/ee/administration/high_availability/gitlab.html)

## Computers

| **Computer Name** |       **DNS Name**        | **IP Address** |
| :---------------: | :-----------------------: | :------------: |
|  prod-gitlab-wa01   | prod-gitlab-wa01.[domain.local] |  [prod-gitlab-wa01 IP]   |
|  prod-gitlab-wa02   | prod-gitlab-wa02.[domain.local] |  [prod-gitlab-wa02 IP]   |
|  prod-gitlab-wa03   | prod-gitlab-wa03.[domain.local] |  [prod-gitlab-wa03 IP]   |

## Prerequisites

[Download/Install](https://about.gitlab.com/installation/#ubuntu) GitLab Omnibus on each node.  The steps are as follows.

1. Install and configure dependencies

    Command:
    ```bash
        sudo apt-get update
        sudo apt-get install -y curl openssh-server ca-certificates
        sudo apt-get install nfs-common
    ```

2. Create the shared directory:

    ```bash
        mkdir -p /media/nfs
    ```

3. Add the GitLab package repository

    Command:
    ```bash
        curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
    ```

4. Install package

    Command:
    ```bash
        sudo apt-get install gitlab-ee
    ```

# Primary GitLab Application Server Configuration

1. Make sure you have the NFS location set up prior to proceeding with web application nodes.

    For this example, we will use the following NFS share `prod-gitlab-nfs01.domain.local:/media/nfs`

    Mount the share by adding the following line to `/etc/fstab`:

    ```bash
        prod-gitlab-nfs01.[domain.local]:/media/nfs/gitlab/ /media/nfs/ nfs4 defaults,soft,rsize=1048576,wsize=1048576,noatime,nofail,lookupcache=positive 0 2
    ```

    Mount the share by running the following command:

    Command:
    ```bash
        sudo mount --all
    ```
    Check to see if the NFS mount is active on your system:

    Command:
    ```bash
        sudo mount -l -t nfs4
    ```

    The output should look like the following:

    ```bash
        prod-gitlab-nfs01.[domain.local]:/media/nfs/gitlab/ on /media/nfs/ type nfs4 (rw,noatime,vers=4.2,rsize=1048576,wsize=1048576,namlen=255,soft,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=[prod-gitlab-wa01 IP],lookupcache=pos,local_lock=none,addr=10.35.4.172)
    ```

2. Add the mount points by adding the following lines to `/etc/fstab` file (don't forget to add the smb credentials to mount the backup location.):

    ```bash
        /media/nfs/git-data /var/opt/gitlab/git-data none bind 0 0
        /media/nfs/.ssh /var/opt/gitlab/.ssh none bind 0 0
        /media/nfs/gitlab-rails/uploads /var/opt/gitlab/gitlab-rails/uploads none bind 0 0
        /media/nfs/gitlab-rails/shared /var/opt/gitlab/gitlab-rails/shared none bind 0 0
        /media/nfs/gitlab-ci/builds /var/opt/gitlab/gitlab-ci/builds none bind 0 0

        #Backup Location
        //prod-backup.domain.local/GitLab/_BACKUP/ /mnt/backups cifs credentials=/cred/.smbcredentials,sec=ntlmsspi,iocharset=utf8,file_mode=0777,dir_mode=0777,noperm
    ```
    Activate mounts:

    Command:
    ```bash
        sudo mount --all
    ```

    If you get an error saying that the mount point does not exist, create the folders by running the following:

    Command:
    ```bash
        mkdir -p /var/opt/gitlab/git-data /var/opt/gitlab/.ssh /var/opt/gitlab/gitlab-rails/uploads /var/opt/gitlab/gitlab-rails/shared /var/opt/gitlab/gitlab-ci/builds
    ```
3. Edit`/etc/gitlab/gitlab.rb`. Replace the content with the following configuration

    ```ruby
        external_url 'https://gitlab.domain.local'

        # Prevent GitLab from starting if NFS data mounts are not available
        high_availability['mountpoint'] = '/var/opt/gitlab/git-data'

        # Disable components that will not be on the GitLab application server
        roles ['application_role']
        nginx['enable'] = true

        # Disable automatic database migrations
        gitlab_rails['auto_migrate'] = false

        # Adjust time zone
        gitlab_rails['time_zone'] = 'America/New_York'

        # Enable HTTPS redirection for nginx web server
        nginx['redirect_http_to_https'] = true

        # LDAP configuration
        gitlab_rails['ldap_enabled'] = true
        ###! **remember to close this block with 'EOS' below**
        gitlab_rails['ldap_servers'] = YAML.load <<-'EOS'
        main: # 'main' is the GitLab 'provider ID' of this LDAP server
        label: 'LDAP'
        host: 'ldap.domain.local'
        port: 636
        uid: 'sAMAccountName'
        method: 'ssl' # "tls" or "ssl" or "plain"
        bind_dn: 'CN=prod-ldap-svc,OU=_Service Accounts,DC=domain,DC=local'
        password: 'Insert the password for the binding account'
        active_directory: true
        allow_username_or_email_login: false
        block_auto_created_users: false
        base: 'OU=Users,DC=domain,DC=local'
        EOS

        postgresql['enable'] = false
        # PostgreSQL connection details
        gitlab_rails['db_host'] = '[prod-gitlab-db01 IP]'# IP/hostname of database servers
        gitlab_rails['db_username'] = 'gitlab'
        gitlab_rails['db_password'] = 'Password for GitLab user in PostgreSQL'
        gitlab_rails['db_port'] = 6432
        gitlab_rails['auto_migrate'] = false

        # Redis connection details
        gitlab_rails['redis_port'] = '6379'
        gitlab_rails['redis_host'] = '[prod-gitlab-rd01 IP]'# IP/hostname of Redis server
        gitlab_rails['redis_password'] = 'Redis Cluster Password'

        redis['master_name'] = 'gitlab-redis'
        redis['master_password'] = 'Redis Cluster Password'
        gitlab_rails['redis_sentinels'] = [
        {'host' => '[prod-gitlab-cs01 IP]', 'port' => 26379},
        {'host' => '[prod-gitlab-cs02 IP]', 'port' => 26379},
        {'host' => '[prod-gitlab-cs03 IP]', 'port' => 26379}
        ]

        # Ensure UIDs and GIDs match between servers for permissions via NFS
        user['uid'] = 9000
        user['gid'] = 9000
        web_server['uid'] = 9001
        web_server['gid'] = 9001
        registry['uid'] = 9002
        registry['gid'] = 9002

        unicorn['worker_processes'] = 3
        unicorn['worker_timeout'] = 60

        nginx['hsts_max_age'] = 0

        # Prometheus Monitoring
        prometheus['enable'] = true
        prometheus_external_url 'http://[prod-gitlab-wa01 IP]'
        prometheus_monitoring['enable'] = true
        prometheus['listen_address'] = '[prod-gitlab-wa01 IP]:9090'
        node_exporter['enable'] = true
        gitlab_monitor['enable'] = true

        # Email Configuration
        # This server uses Postfix

        # Backup Configuration
        gitlab_rails['backup_upload_connection'] = {
        :provider => 'Local',
        :local_root => '/mnt/backups'
        }

        # The directory inside the mounted folder to copy backups to
        # Use '.' to store them in the root directory
        gitlab_rails['backup_upload_remote_directory'] = 'gitlab_backups'
        # Limiting the backup lifetime to 7 days
        gitlab_rails['backup_keep_time'] = 604800
    ```

4. Finally, reconfigure the primary node by running the following command:

    Command:
    ```Bash
        sudo gitlab-ctl reconfigure
    ```
# Additional GitLab Application Servers Configuration

1. Get the shared secrets generated from the primary GitLab application server `prod-gitlab-wa01.[domain.local]` by opening the file `/etc/gitlab/gitlab-secrets.json`

2. Configure the secondary nodes by adding the four lines to the gitlab.rb file (the secrets will be in the json file mentioned before):

    ```ruby
        gitlab_shell['secret_token'] = ''
        gitlab_rails['otp_key_base'] = ''
        gitlab_rails['secret_key_base'] = ''
        gitlab_rails['db_key_base'] = ''
    ```
3. Reconfigure gitlab by running the following command on the secondary nodes:

    Command:
    ```Bash
        sudo gitlab-ctl reconfigure
    ```
