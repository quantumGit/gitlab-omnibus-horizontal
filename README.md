# GitLab-Omnibus Horizontal Playbook

Step by step on how to stand up a horizontal GitLab on-premise infrastructure. For more information, please visit [GitLab's documentation](https://docs.gitlab.com/ee/administration/high_availability/#high-availability-architecture-examples).

## Architectural overview

![GitLab-Horizontal-Diagram](Docs/gitlab-horizontal.PNG)


The GitLab's horizontal design is the simplest form of high availability that allows easy scaling and requires the fewest number of individual virtual servers.

The design requires the following servers to be created:

* 3 PostgreSQL nodes
* 3 Redis nodes
* 3 Consul/Sentinel nodes
* 3 GitLab application nodes (Unicorn, Workhorse, Sidekiq)
* 1 NFS server/appliance


### PostgreSQL

PostgreSQL, often simply Postgres, is an object-relational database management system (ORDBMS) with an emphasis on extensibility and standards compliance.

### Redis

Redis is an open-source in-memory data structure project implementing a distributed, in-memory key-value database with optional durability. Redis supports different kinds of abstract data structures, such as strings, lists, maps, sets, sorted sets, hyperlog logs, bitmaps and spatial indexes.

### Consul

Used for service discovery, to alert other nodes when failover occurs in the database.

### Sentinel

Used for service discovery, to alert other nodes when failover occurs in the Redis cluster.

# Steps

Follow the steps to create a horizontal GitLab infrastructure or learn more about how GitLab is configured:

* [Create VMs based on the specs](Docs/Machine_Specs.md)
* Configure VMs based on the parameters specified
    * [Consul/Sentinel servers](Docs/Consul-Sentinel_Server_Setup.md)
    * [PostgreSQL servers](Docs/PostgreSQL_Server_Setup.md)
    * [Redis servers](Docs/Redis_Server_Setup.md)
    * [Application servers](Docs/WebApplication_Server_Setup.md)
* Setup GitLab-Omnibus on each server
    * [Consul/Sentinel servers](Docs/Consul-Sentinel_Role_Setup.md)
    * [PostgreSQL servers](Docs/PostgreSQL_Role_Setup.md)
    * [Redis servers](Docs/Redis_Role_Setup.md)
    * [Application servers](Docs/WebApplication_Role_Setup.md)
* Configure firewall rules for GitLab-Omnibus on each server
    * [Consul/Sentinel servers](Docs/Consul-Sentinel_Network_Setup.md)
    * [PostgreSQL servers](Docs/PostgreSQL_Network_Setup.md)
    * [Redis servers](Docs/Redis_Network_Setup.md)
    * [Application servers](Docs/WebApplication_Network_Setup.md)
* Although it is beyond the scope of this documentation, make sure you configure your load balancer (NetScaler, F5, etc.)